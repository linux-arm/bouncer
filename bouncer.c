// SPDX-License-Identifier: GPL-2.0-only
/* Copyright (C) 2019  Arm Limited */

#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <elf.h>
#include <endian.h>
#include <ucontext.h>
#include <unistd.h>
#include <sys/auxv.h>
#include <sys/prctl.h>
#include <linux/audit.h>
#include <linux/filter.h>
#include <linux/seccomp.h>
#include <asm/unistd.h>

#include "bouncer.h"

static volatile sig_atomic_t istty = 0;
long _hidden (*__kernel_syscall)(int nr, ...) = NULL;

static void sys_handler(int n _unused, siginfo_t *si _unused, void *uc_)
{
	ucontext_t *uc = uc_;
	unsigned int nr;

	nr = uc->uc_mcontext.regs[8];

	switch (nr) {
	case __NR_rt_sigaction:
		if (do_rt_sigaction(uc))
			return;
		break;

	case __NR_rt_sigprocmask:
		if (do_rt_sigprocmask(uc))
			return;
		break;

	case __NR_prctl:
		if (do_prctl(uc))
			return;
		break;
	}

	uc->uc_mcontext.regs[0] = __kernel_syscall(nr,
		uc->uc_mcontext.regs[0],
		uc->uc_mcontext.regs[1],
		uc->uc_mcontext.regs[2],
		uc->uc_mcontext.regs[3],
		uc->uc_mcontext.regs[4],
		uc->uc_mcontext.regs[5],
		uc->uc_mcontext.regs[6]);
}

#define OFFSET(p, member) ((size_t)((char *)&(p)->member - (char *)(p)))

/*
 * Look up __kernel_syscall() manually in the vDSO.
 * This avoids initialisation order / shared library search order issues
 * during libc startup.  There is probably a better way to do this.
 */

#define AUXV_MAX 64	/* arbitrary sanity limit on the length of auxv */
static int lookup___kernel_syscall(void)
{
	FILE *f;
	unsigned long auxv[AUXV_MAX][2];
	long n;
	unsigned long base;
	const Elf64_Ehdr *e;
	size_t b, i, dynnr, dyn_limit = 0;;
	const Elf64_Dyn *dyn = NULL;
	const char *strtab = NULL;
	size_t symsz = 0, strsz = 0;

	/* Apparently, getauxval() may not work yet.  Try stdio instead. */

	f = fopen("/proc/self/auxv", "rb");
	if (!f)
		goto error;

	if (fread(auxv, sizeof(*auxv), AUXV_MAX, f) < 1) {
		fclose(f);
		goto error;
	}

	fclose(f);

	n = -1;
	for (i = 0; i < AUXV_MAX; ++i) {
		if (auxv[i][0] == AT_NULL)
			break;

		if (auxv[i][0] == AT_SYSINFO_EHDR) {
			n = auxv[i][1];
			break;
		}
	}

	if (n == -1)
		goto error;

#if 0
	n = getauxval(AT_SYSINFO_EHDR);
	if (n == -1 && errno) {
		perror("AT_SYSINFO_EHDR");
		goto error;
	}
#endif

	base = n;
	e = (const Elf64_Ehdr *)n;
	if (e->e_machine != EM_AARCH64)
		goto error;

	if (e->e_ident[EI_CLASS] != ELFCLASS64)
		goto error;

	switch (e->e_ident[EI_DATA]) {
	case ELFDATA2LSB:
		if (__BYTE_ORDER != __LITTLE_ENDIAN)
			goto error;
		break;

	case ELFDATA2MSB:
		if (__BYTE_ORDER != __BIG_ENDIAN)
			goto error;
		break;
	}

	b = base + e->e_phoff;
	for (i = 0; i < e->e_phnum; ++i, b += e->e_phentsize) {
		const Elf64_Phdr *p = (const Elf64_Phdr *)b;

		if (p->p_type == PT_DYNAMIC) {
			if (dyn)
				goto error;

			dyn = (const Elf64_Dyn *)(base + p->p_offset);
			dynnr = p->p_filesz / sizeof(*dyn);
			dyn_limit = base + p->p_offset + p->p_filesz;
		}
	}

	if (!dyn)
		goto error;

	b = 0;
	for (i = 0; i < dynnr; ++i) switch (dyn[i].d_tag) {
	case DT_STRTAB:
		if (strtab)
			goto error;
		strtab = (const char *)(base + dyn[i].d_un.d_ptr);
		break;

	case DT_SYMTAB:
		if (b)
			goto error;
		b = base + dyn[i].d_un.d_ptr;
		break;

	case DT_STRSZ:
		if (strsz)
			goto error;
		strsz = base + dyn[i].d_un.d_val;
		break;

	case DT_SYMENT:
		if (symsz)
			goto error;
		symsz = dyn[i].d_un.d_val;
		break;
	}

	if (!strtab || !b || !strsz || !symsz)
		goto error;

	if (b > dyn_limit)
		goto error;

	while (symsz <= dyn_limit - b) {
		const Elf64_Sym *s = (const Elf64_Sym *)b;

		if (s->st_name >= strsz)
			break;

		if (!strcmp(strtab + s->st_name, "__kernel_syscall")) {
			__kernel_syscall = (long (*)(int, ...))(
				base + s->st_value);
			break;
		}

		b += symsz;
	}

	if (__kernel_syscall)
		return 0;

error:
	return -1;
}

static void _constructor init(void)
{
	struct sigaction sa, oldsa;
	static const struct seccomp_data sd;
	sigset_t oldmask;

	if (lookup___kernel_syscall())
		return;

	init_sigaction();

#define LD(member, offset)						\
	BPF_STMT(BPF_LD|BPF_ABS|BPF_W, OFFSET(&sd, member) + (offset))
#define JNE(k, jt)					\
	BPF_JUMP(BPF_JMP|BPF_JEQ|BPF_K, k, 0, jt)
#define JGTX(jt)					\
	BPF_JUMP(BPF_JMP|BPF_JGT|BPF_X, 0, jt, 0)
#define JLTX(jt)					\
	BPF_JUMP(BPF_JMP|BPF_JGE|BPF_X, 0, 0, jt)
#define JLEX(jt)					\
	BPF_JUMP(BPF_JMP|BPF_JGT|BPF_X, 0, 0, jt)
#define RET(k)					\
	BPF_STMT(BPF_RET|BPF_K, k)
#define TAX					\
	BPF_STMT(BPF_MISC|BPF_TAX, 0)

	static const struct sock_filter insns[] = {
		LD(arch, 0),
		JNE(AUDIT_ARCH_AARCH64, 19 /*trap*/),

		LD(instruction_pointer, 4),
		TAX,
		LD(ip_bounds[0], 4),
		JGTX(15 /*trap*/),
		JLTX(4 /*pass0*/),

		LD(instruction_pointer, 0),
		TAX,
		LD(ip_bounds[0], 0),
		JGTX(10 /*trap*/),

	/*pass0*/
		LD(instruction_pointer, 4),
		TAX,
		LD(ip_bounds[1], 4),
		JLTX(6 /*trap*/),
		JGTX(4 /*pass1*/),

		LD(instruction_pointer, 0),
		TAX,
		LD(ip_bounds[1], 0),
		JLEX(1 /*trap*/),

	/*pass1*/
		RET(SECCOMP_RET_ALLOW),

	/*trap*/
		RET(SECCOMP_RET_TRAP),
	};

	static const struct sock_fprog prog = {
		.len = sizeof(insns) / sizeof(*insns),
		.filter = (struct sock_filter *)insns
	};

	sa.sa_sigaction = sys_handler;
	sa.sa_flags = SA_SIGINFO | SA_NODEFER;
	if (sigemptyset(&sa.sa_mask))
		return;

	if (sigaction(SIGSYS, &sa, &oldsa))
		return;

	if (sigaddset(&sa.sa_mask, SIGSYS))
		goto error_oldsa;

	if (sigprocmask(SIG_UNBLOCK, &sa.sa_mask, &oldmask))
		goto error_oldsa;

	if (prctl(PR_SET_SECCOMP, SECCOMP_MODE_FILTER_UNTIL_EXEC, &prog))
		goto error_oldmask;

	return;

	/* If something went wrong, try to recover and carry on... */
error_oldmask:
	sigprocmask(SIG_SETMASK, &oldmask, NULL);
error_oldsa:
	sigaction(SIGSYS, &oldsa, NULL);
}
