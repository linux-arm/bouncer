CROSS_COMPILE =
CC = $(CROSS_COMPILE)gcc

BUILD_LDFLAGS = -Wl,-zdefs -shared $(LDFLAGS)
BUILD_CFLAGS = -Wall -Wextra -O2 -g -pthread -fPIC $(CFLAGS)
BUILD_CPPFLAGS = $(CPPFLAGS) -I.

TARGETS = bouncer.so

all: FORCE $(TARGETS) ;

FORCE: ;

bouncer.so: bouncer.o fallback.c siglock.c
	$(CC) $(BUILD_CFLAGS) $(BUILD_LDFLAGS) -o bouncer.so $^

.c.o:
	$(CC) $(BUILD_CPPFLAGS) $(BUILD_CFLAGS) -c -o $@ $<

clean: FORCE
	$(RM) *.o $(TARGETS)
