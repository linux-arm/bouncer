// SPDX-License-Identifier: GPL-2.0-only
/* Copyright (C) 2019  Arm Limited */

#include <pthread.h>

#include "bouncer.h"

static pthread_mutex_t sighand_lock = PTHREAD_MUTEX_INITIALIZER;

void _hidden siglock(void)
{
	pthread_mutex_lock(&sighand_lock);
}

void _hidden sigunlock(void)
{
	pthread_mutex_unlock(&sighand_lock);
}
