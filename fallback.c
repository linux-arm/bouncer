// SPDX-License-Identifier: GPL-2.0-only
/* Copyright (C) 2019  Arm Limited */

#include <stddef.h>
#include <linux/types.h>
typedef __kernel_size_t size_t;

#include <linux/errno.h>
#include <linux/prctl.h>
#include <linux/signal.h>
#include <asm/sigcontext.h>
#include <asm/ucontext.h>
#include <asm/unistd.h>

#include "bouncer.h"

/* Internal sigaction table: must be accessed under sighand_lock */
struct sigaction actions[_NSIG - 1];

/*
 * Set up internal sigaction table from the kernel.
 * This succeeds: if there's a failure then something pretty bad is
 * going on, so just kill ourselves in that case.
 */
void _hidden init_sigaction(void)
{
	unsigned int i;

	for (i = 1; i < _NSIG; ++i)
		if (__kernel_syscall(__NR_rt_sigaction, i, NULL,
				     &actions[i - 1],
				     sizeof actions[i - 1].sa_mask))
			__kernel_syscall(__NR_kill,
					 __kernel_syscall(__NR_getpid),
					 SIGKILL);
}

/*
 * Signal delivery and sigaction emulation
 *
 * The purpose of this code is to hide SIGSYS from the user code:
 * sigaction(SIGSYS, ...) will succeed, but the user's handler is never
 * called.
 * For other signals, call the user's handler with the usual semantics,
 * but prevent masking of SIGSYS on return via uc_sigmask.
 */
typedef void (*sigaction_func)(int, siginfo_t *, void *);
typedef void (*sighandler_func)(int);

static void handler(int n, siginfo_t *si, void *uc_)
{
	struct ucontext *uc = uc_;
	struct sigaction act;
	sigaction_func sa_sigaction_fn;

	siglock();
	act = actions[n - 1];
	if (act.sa_flags & SA_RESETHAND) {
		actions[n - 1].sa_flags = 0;
		actions[n - 1].sa_handler = SIG_DFL;
	}
	sigunlock();

	sa_sigaction_fn = (sigaction_func)(unsigned long)act.sa_handler;
	if (act.sa_flags & SA_SIGINFO)
		sa_sigaction_fn(n, si, uc);
	else
		act.sa_handler(n);

	/* Ensure SIGSYS stays unmasked on return */
	uc->uc_sigmask.sig[0] &= ~(1UL << (SIGSYS - 1));
}

int _hidden do_rt_sigaction(void *uc_)
{
	long res;
	struct ucontext *uc = uc_;
	int n = (int)uc->uc_mcontext.regs[0];
	const struct sigaction *new =
		(const struct sigaction *)uc->uc_mcontext.regs[1];
	struct sigaction *old = (struct sigaction *)uc->uc_mcontext.regs[2];
	struct sigaction tmp, tmp2;

	if (n < 1 || n >= _NSIG) {
		res = -EINVAL;
		goto out;
	}

	if (n == SIGSYS) {
		/* Fake it */

		siglock();
		if (old)
			tmp = actions[SIGSYS - 1];
		if (new)
			actions[SIGSYS - 1] = *new;
		if (old)
			*old = tmp;
		sigunlock();

		res = 0;
		goto out;
	}

	/* Other signals: establish as requested, but via handler() */

	siglock();
	if (old)
		tmp = actions[n - 1];

	if (new) {
		tmp2 = *new;
#ifdef SA_RESTORER
		if (tmp2.sa_flags & SA_RESTORER) {
			res = -EINVAL;
			goto out_unlock;
		}
#endif
		actions[n - 1] = tmp2;
	}

	if (old)
		*old = tmp;
	sigunlock();

	if (tmp2.sa_handler != SIG_DFL ||
	    tmp2.sa_handler != SIG_IGN) {
		tmp2.sa_flags |= SA_SIGINFO;
		tmp2.sa_handler = (sighandler_func)(unsigned long)handler;
	}

	res = __kernel_syscall(__NR_rt_sigaction, n, &tmp2, NULL,
			       sizeof tmp2.sa_mask);
	goto out;

out_unlock:
	sigunlock();
out:
	uc->uc_mcontext.regs[0] = res;
	return 1;
}

/*
 * rt_sigprocmask() emulation
 *
 * Since we are called from the SIGSYS handler that intercepted the
 * initial rt_sigprocmask syscall, we to operate on that handler's
 * uc_sigmask, not the real signal mask.
 * Either way, the user is not allowed to mask SIGSYS: ensure that
 * always ends up unmasked.
 */
int _hidden do_rt_sigprocmask(void *uc_)
{
	struct ucontext *uc = uc_;
	int how = (int)uc->uc_mcontext.regs[0];
	const sigset_t *mask = (const sigset_t *)uc->uc_mcontext.regs[1];
	sigset_t *old = (sigset_t *)uc->uc_mcontext.regs[2];
	unsigned int i;

	/*
	 * Simulate the desired mask change, to take effect on sigreturn.
	 * Ensure SIGSYS stays unmasked.
	 */

	if (old)
		*old = uc->uc_sigmask;

	if (!mask)
		goto out;

	switch (how) {
	case SIG_BLOCK:
		for (i = 0; i < _NSIG_WORDS; ++i)
			uc->uc_sigmask.sig[i] |= mask->sig[i];
		break;

	case SIG_UNBLOCK:
		for (i = 0; i < _NSIG_WORDS; ++i)
			uc->uc_sigmask.sig[i] &= ~mask->sig[i];
		break;

	case SIG_SETMASK:
		uc->uc_sigmask = *mask;
		break;

	default:
		/* Bad request */
		uc->uc_mcontext.regs[0] = -EINVAL;
		return 1;
	}

	uc->uc_sigmask.sig[0] &= ~(1UL << (SIGSYS - 1));

out:
	uc->uc_mcontext.regs[0] = 0;
	return 1;
}

/* Emulate prctl() to forbid use of SECCOMP */
int _hidden do_prctl(void *uc_)
{
	struct ucontext *uc = uc_;
	int cmd = (int)uc->uc_mcontext.regs[0];

	switch (cmd) {
	case PR_SET_SECCOMP:
	case PR_GET_SECCOMP:
		uc->uc_mcontext.regs[0] = -EINVAL;
		return 1;
	}

	return 0; /* fall back to real prctl syscall */
}
