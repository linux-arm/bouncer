/* SPDX-License-Identifier: GPL-2.0-only */
/* Copyright (C) 2019  Arm Limited */

#ifndef BOUNCER_H
#define BOUNCER_H

#define _unused __attribute__((__unused__))
#define _hidden __attribute__((__visibility__("hidden")))
#define _constructor __attribute__((__constructor__))

long _hidden (*__kernel_syscall)(int nr, ...);

void init_sigaction(void);
int do_rt_sigaction(void *uc_);
int do_rt_sigprocmask(void *uc_);
int do_prctl(void *uc_);

void siglock(void);
void sigunlock(void);

#endif /* ! BOUNCER_H */
